<?php get_header(); ?>

	<section id="slider" class="swiper_wrapper clearfix" data-speed="600" data-loop="true" data-autoplay="8000" data-grab="true">

		<div class="swiper-container swiper-parent">
			<div class="swiper-wrapper">
				<?php foreach ( ( get_field ('bildspel') ) as $slideshow ): ?>
				<div class="swiper-slide" style="background-image: url('<?php echo $slideshow['bild']['sizes']['slide-img'];?>');">
					<div class="container clearfix">
						<div class="slider-caption slider-caption-center">
							<span class="fadeInUp animated" data-caption-delay="200" data-animate="fadeInUp"><?php echo $slideshow['text']; ?></span>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="hidden-xs">
				<div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
				<div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
			</div>
		</div>
	
		<div class="swiper-pagination"></div>

	</section>
	
	<?php get_template_part('parts/part-book'); ?>
	
	<?php get_template_part('parts/part-bottom-boxes'); ?>
	

<?php get_footer(); ?>