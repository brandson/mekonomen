<?php

add_action( 'after_switch_theme', 'bson_setup' );
 
 
function bson_setup() {
	
	
	// Add's thank you page
	$post_thank_you = array(
	  'post_content'   => "<h1>Tack för ditt intresse</h1><p>Vi återkommer till dig inom kort!</p>",
	  'post_title'     => "Tack för ditt intresse",
	  'post_status'    => "publish",
	  'post_type'      => "page",
	  'ping_status'    => 'closed',
	  'menu_order'     => "100",
	  'comment_status' => "closed"
	); 
	wp_insert_post( $post_thank_you );
	
	
	// Update example page to startsida
	$post_home = array(
	  'ID'             => 2,
	  'post_name'      => "startsida",
	  'post_title'     => "Startsida",
	  'post_type'	   => 'page',
	  'post_status'    => 'publish',
	); 
	$update_home = wp_insert_post( $post_home );
	
	if( $update_home ) {
		update_option( 'show_on_front', 'page' );
        update_option( 'page_on_front', 2 );
	}
	
	register_nav_menu( 'start', 'Start' );
		
}



?>