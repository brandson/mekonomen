<?php

function form_db_save_form() {
	global $wpdb;
	$table_name = $wpdb->prefix ."_form_save_db_data";
	
	_form_db_create_db();
	
	$post = $_POST;
	
	$message = '';
	foreach($post as $row => $v) {
		$message .= '<b>'. strtoupper($row) .'</b> <br />'. strip_tags(nl2br($v)) .' <br /><br />';
	}
	
	
	$insert = array(
		'form_data' => $message,
		'new' => 1,	
	);
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'form_data' => $message, 
			'new' => 1 
		)
	);
	
}


function _form_db_create_db() {
	
	global $wpdb;
	$table_name = $wpdb->prefix ."_form_save_db_data";
	
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		
	     //if it does not exists we create the table
	    $charset_collate = $wpdb->get_charset_collate();
	    $sql="CREATE TABLE `$table_name`(
			`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			`form_data` text,
			`new` int(11) DEFAULT '0',
			`date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)
	    ) $charset_collate;";
	
	    //wordpress function for updating the table
	    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	    
	    dbDelta( $sql );
	}
	
}

add_action('admin_menu', 'my_cool_plugin_create_menu');

function my_cool_plugin_create_menu() {
	global $wpdb;
	
	
	$table_name = $wpdb->prefix ."_form_save_db_data";
	
	
	$forms = $wpdb->get_results( 
		"
		SELECT *
		FROM $table_name
		WHERE new = 1
		"
	);
	
	$count = count($forms);
	
	$name = '';
	if( $count > 0 ) {
		$name = ' <span class="update-plugins count-'. $count. '"><span class="plugin-count">'. $count .'</span></span>';
	}
	
	//create new top-level menu
	add_menu_page('Formulär', 'Formulär'. $name, 'administrator', __FILE__, 'form_db_admin_view', 'dashicons-book-alt', 85);

}


function form_db_admin_view() {
	
	global $wpdb;
	$table_name = $wpdb->prefix ."_form_save_db_data";
	
	$forms = $wpdb->get_results( 
		"
		SELECT *
		FROM $table_name
		ORDER BY date_time DESC
		"
	);
	
	include('form_db_admin_view.php');
	
	foreach( $forms as $form ) {
		$wpdb->update( 
			$table_name, 
			array( 
				'new' => 0,
			),
			array( 'id' => $form->id )	
		);
	}
	
	
}
?>