<div class="book block">

	<div class="container">
	
		<div class="row">
			
			<div class="col-md-12 col-sm-12">
			
				<h1>Boka tid i vår bilverkstad <br>för bilservice eller reparation</h1>
			
			</div>
		
			<div class="col-md-4 col-sm-6 col-xs-12">
				
				<div class="service">
			
					<?php the_field('punkter', 2); ?>
				
				</div>
			
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12">
				
				<div class="contact">
					
					<i class="fa fa-map-marker"></i>
					
					<div class="adress">
				
						<?php the_field('foretagsnamn', 'options'); ?><br/>
						
						<?php the_field('adress', 'options'); ?>, <?php the_field('postadress', 'options'); ?><br/>
						
					</div>
					
					<div class="phone">
						
						<i class="fa fa-phone"></i> <?php the_field('telefonnummer', 'options'); ?><br/>
						
						<i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('epostadress', 'options'); ?>"><?php the_field('epostadress', 'options'); ?></a><br/>
					
					</div>
					
					<a href="https://goo.gl/maps/ReBBKNqCAyS2" target="_blank"><i class="fa fa-map" aria-hidden="true"></i> Karta »</a>
				
				</div>
			
			</div>
			
			<div class="col-md-4 col-sm-12 col-xs-12">
			
		     	<form action="<?php echo get_permalink(4); ?>" method="post" class="form">
					<p class="visible-sm">Fyll i formuläret nedan så kontaktar vi dig inom kort under ordinarie öppettider (08.00-17.00).</p>
			     	<div class="form-group">
				     	<div class="required-field">
					     	<input type="text" name="url"/>
				     	</div>
				     	<div class="row">
					     	<div class="col-md-12 col-sm-12 col-xs-12">
							    <input type="text" class="form-control" name="Namn" placeholder="För- & efternamn" required />
								<input type="text" class="form-control" name="Regnr" placeholder="Reg.nr" required />						     	
						     	<input type="text" class="form-control" name="Telefon" placeholder="Telefon" required />
						     	<input type="text" class="form-control" name="Epostadress" placeholder="E-post" required />
						     	<input type="submit" class="btn btn-yellow" value="Boka"/>
					     	</div>
					     	<input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="Sida" />								     									     									     	
				     	</div>
			     	</div>
		     	</form>
			
			</div>
		
		</div>
	
	</div>

</div>

<div class="clearfix"></div>