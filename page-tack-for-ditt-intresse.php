<?php global $post; $id = $post->ID; ?>
<?php 
/**
 * @package WordPress
 * @subpackage Default_Theme
 
 *Updates:
 	2012-07-31: Added $email_check, usage against spam. // Erik
 	2014-05-20: Added $email_check WITH EMPTY FIELD, usage against spam. // Erik
 */

$string = $_POST['Epostadress'];
$check_empty = $_POST['url'];

if (preg_match('/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/', $string)) { 
	$email_check = true;
} else {
	$email_check = false;
}


if( $check_empty != '' ) {
	$email_check = false;
}
unset( $_POST['url'] );


$message = '
<p>Detta är ett mail genererat från att av dina kontaktformulär</p>
<h3>Kontaktuppgifter</h3> ';

foreach($_POST as $row => $v) {
	$message .= '<b>'. strtoupper($row) .'</b> <br />'. strip_tags(nl2br($v)) .' <br /><br />';
}
/*
$message .= '
	<h3>Kontaktuppgifter</h3>
	<b>Kontaktperson</b><br />
	'. $name .'<br /><br />
	<b>Telefonnummer</b><br />
	'. $phone .'<br /><br />
	<b>E-post</b><br />
	'. $email .' <span style="font-size:10px; font-style:italic">(Svara mailet så svarar du direkt på denna adress)</span><br /><br />
';
*/

$message .= '	------------------------------------------------------------------------------------------------------------------<br />
	<br />
	För frågor eller funderingar kontakta Brandson på 08 - 36 19 38 eller via traffic@brandson.se
	</em>';


if( function_exists('get_field') ) {
	$to = get_field( 'epost_form', 'options' );
}

$to = 'boka@mekonomennorrtull.se';

// subject
$subject = 'Kontaktformulär via '. str_replace('http://www.','', get_option('siteurl'));

// message


// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

// Additional headers
$headers .= 'BCC: Traffic Brandson <all@brandson.se>' . "\r\n";
$headers .= 'From: Kontaktformulär <'. $to .'>' . "\r\n";
$headers .= 'Reply-To: '. $_POST['Namn'] .' <'. $_POST['Epostadress'] .'>' . "\r\n";

// Mail it

if( $email_check ) {
	wp_mail($to, $subject, $message, $headers);
	
	if(function_exists('form_db_save_form')) {
		form_db_save_form();
	}
}



?>
<?php include(TEMPLATEPATH .'/page.php'); ?>