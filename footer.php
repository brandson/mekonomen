	<div class="footer">
		
		<div class="container">
			
			<div class="content">
		
				<div class="row">
				
					<div class="col-md-4 col-sm-4">
					
						<span class="head"><?php the_field('foretagsnamn', 'options'); ?></span><br/>
						
						<a href="#"><i class="fa fa-map-marker"></i></a>
						<p class="map">
						<?php the_field('adress', 'options'); ?><br/>
						<?php the_field('postnummer', 'options'); ?> <?php the_field('postadress', 'options'); ?><br/>
						</p>
						
						<p><i class="fa fa-phone"></i> <?php the_field('telefonnummer', 'options'); ?><br/>
						 <a href="mailto:<?php the_field('epostadress', 'options'); ?>"><i class="fa fa-envelope"></i> <?php the_field('epostadress', 'options'); ?></a>
					
					</div>
					
					<div class="col-md-4 col-sm-4">
					
						<span class="head">Öppettider</span><br/>
						
						<p>
						Mån-fre: <?php the_field('oppet1', 'options'); ?><br/>
						Lördagar: <?php the_field('oppet2', 'options'); ?><br/>
						Söndagar: <?php the_field('oppet3', 'options'); ?><br/>
						<strong>Sommaröppet i Augusti:</strong><br/>
						Mån-fre: 07.00-16.00<br/>
						</p>
					
					</div>
					
					<div class="col-md-4 col-sm-4">
						
						<span class="head">Vad tycker våra kunder?</span><br/>
						
						<iframe src="https://widget.reco.se/v2/widget/3549329?mode=SMALL" width="300" height="150" style="border:0;display:block;"></iframe>
					
					</div>
				
				</div>
			
			</div>
			
			<div class="row">
			
				<div class="col-md-12">
					
					<div class="book-btn">
					
						<a href="<?php echo get_permalink(18); ?>" class="btn btn-yellow">Boka tid för service/reparation</a>
					
					</div>
				
				</div>
			
			</div>
		
		</div>
	
	</div>	
	
	<div class="copy">
	
		<a href="https://www.brandson.se" class="extra" target="_blank">Webbyrå: Brandson AB i Stockholm</a> | <a href="<?php echo get_permalink(111); ?>">Sitemap</a>
	
	</div>
	
	</div><!-- #wrapper end -->

   
	
	
	<!-- end footer -->
	<?php wp_footer(); ?>

	
	<!-- Footer Scripts
    ============================================= -->
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/functions.js"></script>
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/scripts.js"></script>

	<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
	<script type="text/javascript">
	    window.cookieconsent_options = {"message":"Vi använder oss av Cookies. När du använder vår webbsida godkänner du det.","dismiss":"Jag förstår","learnMore":"Läs mer om cookies »","link":"<?php echo get_permalink(121); ?>","theme":"dark-bottom"};
	</script>
	
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
	<!-- End Cookie Consent plugin -->  

  </body>
</html>