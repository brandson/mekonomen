<?php

include( 'lib/setup.php'); // Load setup
include( 'lib/acf.php' ); // Load Advance Custom Fields
include( 'lib/form_db/form_db.php' );


function my_theme_add_editor_styles() {
    add_editor_style( 'lib/custom-edit-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );


add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'start', __( 'Start', 'brandson-canvas' ) );
}


add_theme_support( 'post-thumbnails' );	


add_action('wp_enqueue_scripts', 'no_more_jquery');
function no_more_jquery(){
    wp_deregister_script('jquery');
}

function add_iframe($initArray) {
$initArray['extended_valid_elements'] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]";
return $initArray;
}
add_filter('tiny_mce_before_init', 'add_iframe');



function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '<p><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Läs vidare »</a></p>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

/******* Customize Login Logo ******/
function my_custom_login_logo() {
    echo '<style type="text/css">
       #login h1 a { background-image:url(https://www.brandson.se/download/brandson-logo.png) !important; background-size: 296px 88px !important; height: 88px !important; width: 296px !important; }
    </style>';
}
add_action('login_head', 'my_custom_login_logo');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}if( function_exists('acf_set_options_page_title') )
{
    acf_set_options_page_title( __('Kontaktuppgifter') );
}

add_image_size( 'page-img', 2000, 300, true );
add_image_size( 'slide-img', 2000, 500, true );

?>
