<div class="bottom-boxes clearfix">
	<div class="image"></div>
	<div class="box-overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="text">
					<span><?php the_field('rubrik_1', 2); ?></span><br>
					<?php the_field('textbox_1', 2); ?>
					<a href="<?php echo get_permalink(74); ?>" class="btn btn-yellow">Läs mer</a>
				</div>
			</div>
			<div class="box2">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="text2">
								<span><?php the_field('rubrik_2', 2); ?></span><br>
								<?php the_field('textbox_2', 2); ?>
								<a href="<?php echo get_permalink(12); ?>" class="btn btn-yellow">Läs mer</a>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
