<!DOCTYPE html>
<html>
	<head>
		<title><?php wp_title(); ?></title>
		<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
		
		<!-- Bootstrap -->
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.css" type="text/css" />
	    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/font-icons.css" type="text/css" />
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/animate.css" type="text/css" />
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/magnific-popup.css" type="text/css" />
	
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/responsive.css" type="text/css" />
	   
	    
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	    <!--[if lt IE 9]>
	    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	    <![endif]-->
	    
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/colors.css" type="text/css" />
	    <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/fonts.css" type="text/css" />
		<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/css/swiper.css" type="text/css" />
	
		<!-- External JavaScripts
	    ============================================= -->
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.js"></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/plugins.js"></script>	

	    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDWFY0DGTnmlwcLGvh8lnektJTTiP98ers"></script>
	    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.gmap.js"></script>
	
	    <!-- Document Title
	    ============================================= -->

		<link href="<?php bloginfo( 'template_url' ); ?>/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/styles.css" />
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/less.js"></script>
		
		<?php wp_head(); ?>
		
		<script type="text/javascript">//<![CDATA[
		// Google Analytics for WordPress by Yoast v4.1.3 | http://yoast.com/wordpress/google-analytics/
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount','UA-26948106-1']);
		_gaq.push(['_trackPageview'],['_trackPageLoadTime']);
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		//]]
		</script>
		
	</head>
	<body class="stretched no-transition">
		
	<!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
	    
	    		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">
				
				<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

				<!-- Logo
				============================================= -->
				<div id="logo">
					<a href="<?php bloginfo( 'url' ); ?>" class="standard-logo"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt="Mekonomen Norrtull Logo"></a>
					<a href="<?php bloginfo( 'url' ); ?>" class="retina-logo"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo@2x.png" alt="Mekonomen Norrtull Logo"></a>
				</div><!-- #logo end -->

				<div class="header-extras">
					
					<span class="contact hidden-sm hidden-xs">Frågor eller boka tid?<br/> Ring oss på</span> <span class="phone tada animated" data-animate="tada"><a href="tel:<?php the_field('telefonnummer', 'options'); ?>"><i class="fa fa-phone"></i></a><span><?php the_field('telefonnummer', 'options'); ?></span></span></span><br/>
					
				</div>

			</div>
			
			<div id="header-wrap hidden-xs">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2">
	
					<div class="container clearfix">
	
						<?php wp_nav_menu( array( 'menu' => 'Huvudmeny', 'container' => '' ) ); ?>
	
	
				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->
