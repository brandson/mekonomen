<?php 
global $post;

if ($post->post_parent)	{
	$ancestors=get_post_ancestors($post->ID);
	$root=count($ancestors)-1;
	$parent = $ancestors[$root];
} else {
	$parent = $post->ID;
}

$children = wp_list_pages("title_li=&child_of=". $parent ."&echo=0"); ?>

<?php if( $children ): ?>

	<div class="menu">
		<ul class="categories">
		
			<span><?php echo get_the_title($parent); ?></span>
			
			<?php echo $children; ?>
		
		</ul>
	</div>

<?php else: ?>

	<div class="open">
		
		<span>Öppettider</span><br/>
		
		Vardagar: 07.00-17.00<br/>
		Lördagar: Stängt<br/>
		Söndagar: Stängt<br/>
		<strong>Sommaröppet i Augusti:</strong><br/>
		Mån-fre: 07.00-16.00<br/>
		
		
		<a href="<?php echo get_permalink(18); ?>" class="btn btn-yellow">Boka tid</a>
	
	</div>

<?php endif; ?>
