<?php

/*
Template Name: Kontakt
*/

get_header(); ?>

<section id="map-overlay" class="contact hidden-sm hidden-xs">

	<div class="container clearfix">

		<!-- Contact Form Overlay
		============================================= -->
		
		<div class="contact-form">
		
			<span>Boka tid för service/reparation</span>
			
			<p>Fyll i formuläret nedan så kontaktar vi dig inom kort under ordinarie öppettider (07.00-17.00).<br/>
				<strong>Sommaröppet i Augusti:</strong><br/>
						Mån-fre: 07.00-16.00</p>
			
	     	<form action="<?php echo get_permalink(4); ?>" method="post" class="form">
		     	<div class="form-group">
			     	<div class="required-field">
				     	<input type="text" name="url"/>
			     	</div>
			     	<div class="row">
				     	<div class="col-md-12 col-sm-12 col-xs-12">
					     	<input type="text" class="form-control" name="Namn" placeholder="För- & efternamn" required />
					     	<input type="text" class="form-control" name="Regnr" placeholder="Reg.nr" required />					     	
					     	<input type="text" class="form-control" name="Telefon" placeholder="Telefon" required />
					     	<input type="text" class="form-control" name="Epostadress" placeholder="E-post" required />
					     	<input type="submit" class="btn btn-yellow" value="Boka"/>
				     	</div>
				     	<input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="Sida" />								     									     									     	
			     	</div>
		     	</div>
	     	</form>
     	
		</div>

	</div>

	<!-- Google Map
	============================================= -->
	<section id="google-map" class="gmap" style="height:100%;overflow: hidden; transform: translateZ(0px);">
		
		
			<script type="text/javascript">
				
			
			    $('#google-map').gMap({
				    address: "Gärdet, Stockholm",
			        maptype: 'ROADMAP',
			        zoom: 13,
			        draggable: false,
			        markers: [ 
			
			        {
			            address: "<?php the_field('adress', 'options'); ?>, <?php the_field('postadress', 'options'); ?>",
			            html: '<div style="width: 300px;"><h1 style="margin-bottom:15px; font-size:30px;">Kontakta oss</h1><p style="font-size:16px"><?php the_field('foretagsnamn', 'options'); ?><br/><?php the_field('adress', 'options'); ?></br><?php the_field('postnummer', 'options'); ?> <?php the_field('postadress', 'options'); ?></p><p style="font-size:16px"><i class="fa fa-phone" style="font-size:18px;"></i>  <?php the_field('telefonnummer', 'options'); ?><br/><i class="fa fa-envelope" style="font-size:18px;"></i> <a style="font-size:16px"  href="mailto:<?php the_field('epostadress', 'options'); ?>"><?php the_field('epostadress', 'options'); ?></a><br/></p></div>',
			            
			            icon: {
			                image: "<?php bloginfo( 'template_url' ); ?>/images/icons/map-icon-red.png",
			                iconsize: [32, 39],
			                iconanchor: [32,39],
			            },
			            popup: true
			        },
			        
			             
			        ],
			        
			        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
			        doubleclickzoom: false,
			        controls: {
			            panControl: true,
			            zoomControl: true,
			            mapTypeControl: true,
			            scaleControl: false,
			            streetViewControl: false,
			            overviewMapControl: false
			        }
			
			    });
			
			</script><!-- Google Map End -->
		
		
	</section>

</section>

<div class="content-block contact visible-sm visible-xs">

	<div class="container">
	
		<div class="row">
			
			<div class="col-sm-6 col-sm-12">
						
			<h1>Kontakta oss</h1>
			<p><?php the_field('foretagsnamn', 'options'); ?><br/>
			<?php the_field('adress', 'options'); ?></br>
			<?php the_field('postnummer', 'options'); ?> <?php the_field('postadress', 'options'); ?></p>
			<p><i class="fa fa-phone"></i><?php the_field('telefonnummer', 'options'); ?><br/>
			<i class="fa fa-envelope" style="font-size:18px;"></i> <a href="mailto:<?php the_field('epostadress', 'options'); ?>"><?php the_field('epostadress', 'options'); ?></a></p>
			
			</div>
			
			<div class="col-sm-6 col-xs-12">
			
				<div class="contact-form contact-sm contact-xs">
				
					<span>Boka tid för service/reparation</span>
					
					<p>Fyll i formuläret nedan så kontaktar vi dig inom kort under ordinarie öppettider (08.00-17.00).</p>
					
			     	<form action="<?php echo get_permalink(4); ?>" method="post" class="form">
				     	<div class="form-group">
					     	<div class="required-field">
						     	<input type="text" name="url"/>
					     	</div>
					     	<div class="row">
						     	<div class="col-md-12 col-sm-12 col-xs-12">
							     	<input type="text" class="form-control" name="Namn" placeholder="Namn" required />
							     	<input type="text" class="form-control" name="Regnr" placeholder="Reg.nr" required />					     	
							     	<input type="text" class="form-control" name="Telefon" placeholder="Telefon" required />
							     	<input type="text" class="form-control" name="Epostadress" placeholder="Epost" required />
							     	<input type="submit" class="btn btn-yellow" value="Boka"/>
						     	</div>
						     	<input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="Sida" />								     									     									     	
					     	</div>
				     	</div>
			     	</form>
		     	
				</div>
			
			</div>
		
		</div>
	
	</div>
</div>


	<!-- Google Map
	============================================= -->
	<section id="google-map2" class="gmap visible-sm visible-xs" style="height:400px;overflow: hidden; transform: translateZ(0px);">
		
		
			<script type="text/javascript">
				
			
			    $('#google-map2').gMap({
			
			        address: '<?php the_field('adress', 'options'); ?>, <?php the_field('postadress', 'options'); ?>',
			        maptype: 'ROADMAP',
			        zoom: 13,
			        draggable: false,
			        scrollwheel: false,
			        markers: [ 
			
			        {
			            address: "<?php the_field('adress', 'options'); ?>, <?php the_field('postadress', 'options'); ?>",
			            html: '<div style="width: 280px;"><h3 style="margin-bottom:15px; font-size:20px;"><?php the_field('foretagsnamn', 'options'); ?></h3><p style="font-size:16px"><?php the_field('adress', 'options'); ?></br><?php the_field('postnummer', 'options'); ?> <?php the_field('postadress', 'options'); ?></p><p style="font-size:16px"><i class="fa fa-phone" style="font-size:18px;"></i>  <?php the_field('telefonnummer', 'options'); ?><br/><i class="fa fa-envelope" style="font-size:18px;"></i> <a style="font-size:16px"  href="mailto:<?php the_field('epostadress', 'options'); ?>"><?php the_field('epostadress', 'options'); ?></a><br/></p></div>',
			            
			            icon: {
			                image: "<?php bloginfo( 'template_url' ); ?>/images/icons/map-icon-red.png",
			                iconsize: [32, 39],
			                iconanchor: [32,39],
			            }
			        },
			        
			             
			        ],
			        
			        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
			        doubleclickzoom: false,
			        controls: {
			            panControl: true,
			            zoomControl: true,
			            mapTypeControl: true,
			            scaleControl: false,
			            streetViewControl: false,
			            overviewMapControl: false
			        }
			
			    });
			
			</script><!-- Google Map End -->
		
		
	</section>
	
	

	
<?php get_template_part('parts/part-bottom-boxes'); ?>


<?php get_footer(); ?>