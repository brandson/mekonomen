# Steg 1
Droppa in mappen "form_db" i mappen "lib" i temat. Om inte mappen "lib" finns så skapa den.

# Steg 2 
Klistra in denna rad överst i functions.php
include( 'lib/form_db/form_db.php' );

# Steg 3
Lägg in denna kod direkt efter wp_mail() (som troligtvis ligger i page-tack-for-ditt-intresse.php) funktionen för att få formulärdatan att sparas. Finns wp_mail() på flera ställen så måste denna kodrad även in där för att spara den datan.

if(function_exists('form_db_save_form')) {
	form_db_save_form();
}

# Steg 4 
Testa skicka ett mail och sedan gå in i admin under "Forumlär" och se så att formuläret blev registrerad.