<?php get_header(); ?>

<?php the_post_thumbnail('page-img', array( 'alt' => get_the_title())); ?>

<div class="breadcrumbs">
	
	<div class="container">
	
		<?php if ( function_exists('yoast_breadcrumb') ) 
		{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
	
	</div>

</div>

	<div class="content-block">
	
		<div class="container">
			
			<div class="row">
			
				<div class="col-md-9 col-sm-6 col-xs-12">
				
					<div class="content">
						
						<?php if (get_field('rubrik')): ?>
						
							<h1><?php the_field('rubrik'); ?></h1>
							
						<?php else: ?>
						
							<h1><?php the_title(); ?></h1>
							
						<?php endif; ?>
					
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>
						
						<?php if (is_page(111)): ?>
						
							<ul>
								<?php wp_list_pages('title_li='); ?>
							</ul>
							
						
						<?php endif; ?>
					
					</div>
				
				</div>
				
				<div class="col-md-3 col-sm-6 col-xs-12">
				
					<div class="sidebar">
					
						<?php get_sidebar(); ?>
					
					</div>
				
				</div>
			
			</div>	
		
		</div>
		
	</div>

	<?php get_template_part('parts/part-book-page'); ?>
	
	<?php get_template_part('parts/part-bottom-boxes'); ?>
	
<?php get_footer(); ?>