<div class="wrap">
<h2><span class="dashicons dashicons-book-alt"></span> Formulär</h2>
<p>Här ser du inkommna formulär från din sajt. Dom som är blåmarkerade är dom nya formulären som har kommit in sedan sist du har var här inne och tittade.</p>
<hr>

<table class="wp-list-table widefat plugins">
	<thead>
	    <tr>
		    <th></th>
	        <th width="20%">Inkommnet</th>
	        <th>Formulär data</th>       
	    </tr>
	</thead>
	<tfoot>
	    <tr>
		    <th></th>
	        <th width="20%">Inkommnet</th>
	        <th>Formulär data</th>       
	    </tr>
	</tfoot>
	<tbody>
	<?php foreach( $forms as $row ): ?>
		<tr class="<?php echo ($row->new) ? 'active' : ''; ?>">
			<th scope="row" class="check-column"></th>
			<td><?php echo $row->date_time; ?></td>
			<td>
				<div class="form_text">
					<a href="#">Visa innehåll</a>
				</div>
				<div class="form_data" style="display:none">
				<?php echo $row->form_data; ?>
				</div>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$('.form_text > a ').click(function() {
		$(this).parent().parent().find('.form_data').toggle();
		return false;
	})
});
</script>
