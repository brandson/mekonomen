<?php get_header(); ?>
<!-- content start -->
<div class="container">
	<div class="row">
		<div id="content" class="col-md-12">
		  <?php if (have_posts()) : ?>
		  <?php while (have_posts()) : the_post(); ?>
		  	
		  	<h2><?php the_title(); ?></h2>
		  	<?php the_excerpt(); ?>
		  	
		  	<hr />	
		 
		 
		  <?php endwhile; ?>
		  
		  <?php else : ?>
		  <h2 class="center">Not Found</h2>
		  <p class="center">Sorry, but you are looking for something that isn't here.</p>
		  <?php include (TEMPLATEPATH . "/searchform.php"); ?>
		  <?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>