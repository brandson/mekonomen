<?php get_header(); ?>

<?php the_post_thumbnail('page-img'); ?>

	<div class="content-block">
	
		<div class="container">
			
			<div class="row">
			
				<div class="col-md-9 col-sm-6 col-xs-12">
				
					<div class="content">
						
						<h1>404: Sidan finns inte</h1>
					
						<p>Sidan du letar efter har antingen blivit flyttad eller borttagen. <a href="<?php bloginfo( 'url' ); ?>">Till startsidan »</a>
					
					</div>
				
				</div>
			
			</div>	
		
		</div>
		
	</div>

	<?php get_template_part('part-book'); ?>
	
	<?php get_template_part('parts/part-bottom-boxes'); ?>
	
<?php get_footer(); ?>